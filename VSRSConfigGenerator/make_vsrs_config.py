#!/usr/bin/python

# Script to generate VSRS configuration file, for premade texture+disparity files
# from 3DLicorneA 170202 acquisition set
# 20 Feb 2017

import sys

def usage():
	print("usage: {} left_camera_index virtual_camera_index right_camera_index synthesis.yuv config.txt\n".format(sys.argv[0]))
	sys.exit(1)

if(sys.argv <= 5):
	usage()

width = 1920
height = 1080
z_near = 700
z_far = 1600

left_idx = int(sys.argv[1])
virtual_idx = int(sys.argv[2])
right_idx = int(sys.argv[3])
output_filename = sys.argv[4]
config_filename = sys.argv[5]

if left_idx == -1 or virtual_idx == -1 or right_idx == -1 or output_filename == '':
	usage()


config = """
# VSRS configuration file generated using make_vsrs_config.py
# For 3DLicorneA 170202 sequence
DepthType                      1                        # 0...Depth from camera, 1...Depth from the origin of 3D space
SourceWidth                    {:d}                      # Input frame width
SourceHeight                   {:d}                      # Input frame height
StartFrame                     0						# Starting frame #
TotalNumberOfFrames            1                      # Total number of input frames 
LeftNearestDepthValue          {:f}              # Nearest depth value of left image from camera or the origin of 3D space
LeftFarthestDepthValue         {:f}             # Farthest depth value of left image from camera or the origin of 3D space
RightNearestDepthValue         {:f}              # Nearest depth value of right image from camera or the origin of 3D space
RightFarthestDepthValue        {:f}             # Farthest depth value of right image from camera or the origin of 3D space
CameraParameterFile            cameras.txt         # Name of text file which includes real and virtual camera parameters
LeftCameraName                 para_cam{:d}               # Name of real left camera
VirtualCameraName              para_cam{:d}             # Name of virtual camera
RightCameraName                para_cam{:d}               # Name of real right camera
LeftViewImageName              texture/Kinect_out_texture_000_500.0z_0001_{:04d}.yuv                 # Name of left input video
RightViewImageName             texture/Kinect_out_texture_000_500.0z_0001_{:04d}.yuv                # Name of right input video
LeftDepthMapName               disparity/Kinect_out_disparity_000_500.0z_0001_{:04d}.yuv           # Name of left depth map video
RightDepthMapName              disparity/Kinect_out_disparity_000_500.0z_0001_{:04d}.yuv           # Name of right depth map video
OutputVirtualViewImageName     {}       # Name of output virtual view video
SynthesisMode                  0                       # 0...General, 1...1D parallel
""".format(
	width,
	height,
	z_near,
	z_far,
	z_near,
	z_far,
	left_idx - 1,
	virtual_idx - 1,
	right_idx - 1,
	left_idx,
	right_idx,
	left_idx,
	right_idx,
	output_filename
)

open(config_filename, 'w').write(config)

print("VSRS configuration written to {}".format(config_filename));
