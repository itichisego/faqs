
DEBUG = False

# ENUMS
class MovementType:
    HORIZONTAL_CONSTANT_NO_ROTATION            = 0
    HORIZONTAL_CONSTANT_AND_ROTATION           = 1
    ROTATION_CONSTANT_NO_HORIZONTAL            = 2
    ROTATION_CONSTANT_AND_HORIZONTAL_MOVEMENT  = 3

    def str(self, movement):
        ret = "not implemented"
        if movement == self.HORIZONTAL_CONSTANT_NO_ROTATION:
            ret = "Horizontal constant no rotation"
        elif movement == self.HORIZONTAL_CONSTANT_AND_ROTATION:
            ret = "Horizontal constant and rotation"
        elif movement == self.ROTATION_CONSTANT_NO_HORIZONTAL:
            ret = "Rotation constant no horizontal"
        elif movement == self.ROTATION_CONSTANT_AND_HORIZONTAL_MOVEMENT:
            ret = "Rotation constant and horizontal movement"

        return ret

import smtplib
class Mail:
    def __init__(self, login, pwd, sender, target):
        self.login = login
        self.pwd = pwd
        self.sender = sender
        self.target = target

    def send(self, message):
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(self.login, self.pwd)

        message = "\n" + message # NECESSARY FOR SMTPLIB, OTHERWISE IT WILL SEND BLANK MESSAGES

        server.sendmail(self.sender, self.target, message)
        server.quit()
