#include <Encoder.h>

#define INPUT_PULLUP

//https://oscarliang.com/arduino-timer-and-interrupt-tutorial/

typedef struct {
  Time() {
    current = last = millis();
  }
  void update() {
    last = current;
    current = millis();
  }
  static unsigned long current = 0;
  static unsigned long last = 0;
} Time;


Time time;


typedef struct {
  Enc(int encoderNumber, int pin1, int pin2, int powerPos, int powerGnd) {
    number = encoderNumber;
    enc = new Encoder(pin1, pin2);
    powerPinPOS = powerPos;
    powerPinGND = powerGND;
  }
  ~Enc() {
    delete enc;
  }
  int number;
  Encoder * enc;
  int powerPinPOS = 0;
  int powerPinGND = 0;

  int32_t currentPosition = 0;
  int32_t lastPosition = 0;

  long frequency = 0;

  void init() {
    // Serial must be set before
    pinMode(powerPinPOS, OUTPUT);
    pinMode(powerPinGND, OUTPUT);
    digitalWrite(powerPinPOS, HIGH);
    digitalWrite(powerPinGND, LOW);
    currentPosition = enc->Read();
    lastPosition = currentPosition;
    frequency = 0;
  }

  void show() {
    update();
    Serial.print("Encoder : ");
    Serial.println(number);
    Serial.print("Position: ");
    Serial.print(currentPosition);
    Serial.print(" Frequency: ");
    Serial.print(frequency);
  }

  void update(Time &time) {
    currentPosition = enc->read();
    frequency = ((currentPosition - lastPosition)*1000.0/(time.current, time.last))
      lastPosition = currentPosition;
  }

  void write(int val) {
    enc->write(val);
  }

  void reset() {
    write(0);
  }

  bool hasFrequency() {
    return abs(frequency) > 0;
  }

} Enc;

Enc encoder1(1, 2, 3, 5, 4);
Enc encoder2(2, 18, 19, 16, 17);  //! order of the pins <> for both arduino connectors


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.flush();

  delay(1.0);
  time.update();

  encoder1.init();
  encoder2.init();
  Serial.println("Encoders initialization: DONE");
}

void loop() {
  // put your main code here, to run repeatedly:

  time.update();
  encoder1.update(time);
  encoder2.update(time);

  if (Serial.available() > 0) {
    int incomingByte = Serial.read();  //read a command (one byte)
    switch (incomingByte) {
      case 's':
        Serial.flush();
        Serial.println(BOARD_ID); 
      case 'r':
        while (Serial.available() == 0) ;
        incomingByte = Serial.read();
        if (incomingByte=='0')  encoder1.reset();
        if (incomingByte=='1')  encoder2.reset();
        encoder1.show();
        encoder2.show();
        break;
      case 'd': 
        encoder1.show();
        encoder2.show();
        break;
    }
  }

  if (encoder1.hasFrequency() || encoder2.hasFrequency()) {
    encoder1.show();
    encoder2.show();
  }

  delay(10);
}
