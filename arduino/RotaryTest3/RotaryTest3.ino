#include <Encoder.h>

#define INPUT_PULLUP

//https://oscarliang.com/arduino-timer-and-interrupt-tutorial/

//mind the space after names
#define BOARD_ID "ID 0002"
#define VAR_ID0 "Z0 " 
#define VAR_ID1 "Z1 " 
#define VAR_ID2 "SZ0 " 
#define VAR_ID3 "SZ1 " 

Encoder myEnc(2,3);
// power for the encoder !
int power_encoder1_pos_pin = 5;
int power_encoder1_gnd_pin = 4;

// power for the encoder !
Encoder myEnc2(18,19);
int power_encoder2_pos_pin = 16; //! order of the pins <> for both arduino connectors
int power_encoder2_gnd_pin = 17;

int32_t pos=0; //rotary encoder position 
int32_t pos2=0;
long lastval=0; 
long freq=0;
long cur=0;
long lastval2=0; 
long freq2=0;
long cur2=0;
unsigned long tlast=0;
unsigned long tcur=0;
bool runmode=false;

void initfreq(){
  delay(1.);
  tcur=millis();
  cur=myEnc.read();
  lastval=cur;
  freq=0;    
  cur2=myEnc2.read();
  lastval2=cur2;
  freq2=0;    
  tlast=tcur;
  Serial.println("init");  
}

void updatefreq() {
    tcur=millis();
    cur=myEnc.read();
    freq=((cur-lastval)*1000)/((long)(tcur-tlast));    
    lastval=cur;
    cur2=myEnc2.read();
    freq2=((cur2-lastval2)*1000)/((long)(tcur-tlast));    
    lastval2=cur2;    
    tlast=tcur;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.flush();
  // power the rotary encoder using pin5
  pinMode(power_encoder1_pos_pin, OUTPUT);
  pinMode(power_encoder1_gnd_pin, OUTPUT);
  pinMode(power_encoder2_pos_pin, OUTPUT);
  pinMode(power_encoder2_gnd_pin, OUTPUT);
  digitalWrite(power_encoder1_pos_pin, HIGH); 
  digitalWrite(power_encoder1_gnd_pin, LOW); 
  digitalWrite(power_encoder2_pos_pin, HIGH); 
  digitalWrite(power_encoder2_gnd_pin, LOW); 
  initfreq();
}

void showdata() {
   pos=myEnc.read(); //rotary encoder position
   pos2=myEnc2.read();
   Serial.print(VAR_ID0);    
   Serial.println(pos);  
   Serial.print(VAR_ID1);    
   Serial.println(pos2);
   Serial.print(VAR_ID2);    
   Serial.println(freq);
   Serial.print(VAR_ID3);    
   Serial.println(freq2);   
}

void loop() {
  // put your main code here, to run repeatedly:
  
  updatefreq();
  if (Serial.available() > 0) {
     int incomingByte = Serial.read();  //read a command (one byte)
      switch (incomingByte) {
         case 's':
            Serial.flush();
            Serial.println(BOARD_ID); 
         case 'r':
           while (Serial.available() == 0) ;
              incomingByte = Serial.read();
              if (incomingByte=='0')  myEnc.write(0);
              if (incomingByte=='1')  myEnc2.write(0);
              runmode = true; // send one event
           break;
         case 'd': 
          showdata();
           break;
         case 't':                              
           runmode=!runmode;
           break;           
      }
  }
  if (runmode) {showdata();runmode = false;}
  if ((abs(freq)>0)||(abs(freq2)>0)) runmode = true;
  if ((abs(freq)==0)&&(abs(freq2)==0)) runmode = false;
  delay(10);
}
