import time

class RemainingTime:
    def __init__(self, endX, horizontalIncrement):
        self.endX = endX
        self.horizontalIncrement = horizontalIncrement
        self.deltaT = 0.0
        self.meanSteps = 0
        self.startTime = 0.0
        self.endTime = 0.0

    def getRemainingTime(self, positionX):
        remainingTime = self._getRemainingTime(positionX)
        h = remainingTime//(60*60)
        m = remainingTime//60-h*60
        s = remainingTime - h*(60*60) - m * 60
        h = int(h)
        m = int(m)
        s = int(s)
        if not (h == 0 and m == 0 and s == 0):
            print("{hours}:{min}:{sec} remaining.".format(hours=int(h), min=int(m), sec=int(s)))

    def _getRemainingTime(self, positionX):
        remainingSteps = (self.endX - positionX) / self.horizontalIncrement
        self._computeDelta()
        return remainingSteps*self.deltaT

    def _computeDelta(self):
        if self.meanSteps == 0:
            self.startTime = time.time()
        elif self.meanSteps == 9:
            self.endTime = time.time()
            self.deltaT = (self.endTime - self.startTime) / 10.0
        self.meanSteps += 1