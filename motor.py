import sys
import serial
import time

from utils import DEBUG

if sys.hexversion >= 0x03000000:
    import _thread as thread
else:
    import thread

class Motor:
    def __init__(self):
        self.x_limits = (5, 980)
        self.y_limits = (5, 170)
        self.z_limits = (100, 995)
        self.centerX = 560# TODO place the right center
        self.x = -1
        self.y = -1
        self.z = -1
        self.onlyX = False
        #self._vOffset = 2 # TODO what is this?
        #self.vx = 20 - self._vOffset
        #self.vy = 10 - self._vOffset
        #self.vz = 10 - self._vOffset
        #self.vSlower = min(self.vx, self.vy, self.vz)
        #self.waitTimeBetweenMovements = 4
        self._ser = serial.Serial("COM3", 57600) # TODO WTF ?, writeTimeout=0)
        time.sleep(3)
        self._absolutePositionMode()
        self._startFan()

    def __del__(self):
        self._stopFan()

    def setX(self, min, max):
        self.x_limits = (min, max)
    def setY(self, min, max):
        self.y_limits = (min, max)
    def setZ(self, min, max):
        self.z_limits = (min, max)

    def setOnlyX(self):
        self.onlyX = True

    #def setOffsets(self, x, y, z):
    #    self.xOffset = x
    #    self.yOffset = y
    #    self.zOffset = z

    #def setWaitTimeBetweenMovements(self, value):
    #    self.waitTimeBetweenMovements = value

    def setArduino(self, motorPort, Port):
        self._ser = serial.Serial(motorPort, 57600)

    def _moveToPosition(self, x, y, z):
        x_dist = abs(self.x - x)
        y_dist = abs(self.y - y)
        z_dist = abs(self.z - z)
        waitingFactor = 0.001
        if z != self.z:
            # We are moving from one row to another
            # We need to reset X and Y
            self.homingX()
            self.homingY()
            self.moveZ(z)
            time.sleep(z_dist * waitingFactor) #mm en 0.1 seconde ? plausible ??

        self.moveX(x)
        time.sleep(x_dist * waitingFactor)
        self.moveY(y)
        time.sleep(y_dist * waitingFactor)

    def homing(self):
        # TODO timers???
        # TODO why those values? 500, 1000, send them via main.py!
        #self.homingY()
        self.homingX()
        if not self.onlyX:
            self.sendToCenterX()
            self.homingZ()
            self.homingX()

    def homingX(self):
        print("Homing X...")
        self._sendToArduino("G28 X")
        time.sleep(55)
        self._sendToArduino("G0 X " + str(self.x_limits[0]))
        self.x = self.x_limits[0]
        #time.sleep(self.x/9.0)
        time.sleep(self.x/15.0 + 3) # 20mm/s (1000mm en 50 secondes

    def setCenterX(self, center):
        self.centerX = center

    def sendToCenterX(self):
        print("Centering X...")
        self._sendToArduino("G0 X " + str(self.centerX))
        self.x = self.centerX
        time.sleep(self.centerX/15.0 + 3) # TODO ajuster la vitesse

    def homingY(self):
        print("Homing Y...")
        # TODO rotation
        #self._sendToArduino("G28 Y")
        #time.sleep(10)
        #self._sendToArduino("G0 Y " + str(self.y_limits[0]))
        self.y = self.y_limits[0]
        time.sleep(10)

    def homingZ(self):
        print("Homing Z...")
        self._sendToArduino("G28 Z")
        time.sleep(100)
        self._sendToArduino("G0 Z " + str(self.z_limits[0]))
        self.z = self.z_limits[0]
        time.sleep((1000-self.z)/9.0)

    def moveX(self, pos):
        self._sendToArduino("G0 X " + str(pos))
        self.x = pos

    def moveY(self, pos):
        if not self.onlyX:
            self._sendToArduino("G0 Y " + str(pos))
        self.y = pos

    def moveZ(self, pos):
        if not self.onlyX:
            self._sendToArduino("G0 Z " + str(pos))
        self.z = pos

    def moveToMin(self):
        # Move to min
        self._moveToPosition(self.x_limits[0], self.y_limits[0], self.z_limits[0])

    def _startFan(self):
        self._sendToArduino("M106")

    def _stopFan(self):
        self._sendToArduino("M107")

    def _absolutePositionMode(self):
        """Set the arduino in absolute position mode (relative to homing position)"""
        self._sendToArduino("G90")

    def _sendToArduino(self, command):
        print(command)
        myCommand = bytes(command, 'utf-8')
        self._ser.write(myCommand + b'\n')
