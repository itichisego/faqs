import os
from os.path import isfile, join
import subprocess

import numpy as np # for accessing elements of a list in a list
import cv2

class VideoMaker(object):
    def __init__(self, folderName, fps):
        self.files = [f for f in os.listdir(folderName) if isfile(join(folderName, f))]
        self.files = [f for f in self.files if ".png" in f]
        self.files = np.array(self.files)
        #self.files.sort()

        self.numberOfPictures = len(self.files)
        self.fps = fps
        self.folderName = folderName

    def _generateSubdividers(self, number):
        return range(0,self.numberOfPictures-1, number)

    def _ffmpg(self,files, mm):
        txt = ""
        for file in files:
            # print('copy ' + file + ' .\\' + thisFolder + "\\" + file)
            # os.system('copy ' + file + ' .\\' + thisFolder + "\\" + file)
            txt = txt + "file '" + file + "'\nduration 1\n"  # + str(1/self.fps) + "\n"

        try:
            os.remove("input")
        except OSError:
            pass

        with open("input", 'w') as f:
            f.write(txt)

        outFileName = "out_1_o_" + str(mm) + "_" + str(self.fps) + "_fps"
        ext = "mp4"
        ffmpeg = "H:\\faqs\\ffmpeg\\ffmpeg.exe"
        params = \
            " -r " + str(self.fps) + \
            " -f concat" + \
            " -i input" + \
            " -c:v libx264" + \
            " -pix_fmt yuv420p" + \
            " " + outFileName + "." + ext

        print(ffmpeg + params)
        subprocess.call(ffmpeg + params, stdout=None, stderr=None, shell=False)

        return outFileName, ext

    def opticalFlow(self,fileName, ext, live=False):
        print("Making Optical Flow")
        cap = cv2.VideoCapture(fileName + "." + ext)
        # params for ShiTomasi corner detection
        feature_params = dict(maxCorners=100,
                              qualityLevel=0.3,
                              minDistance=7,
                              blockSize=7)
        # Parameters for lucas kanade optical flow
        lk_params = dict(winSize=(15, 15),
                         maxLevel=2,
                         criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
        # Create some random colors
        color = np.random.randint(0, 255, (100, 3))
        # Take first frame and find corners in it
        ret, old_frame = cap.read()
        old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
        p0 = cv2.goodFeaturesToTrack(old_gray, mask=None, **feature_params)
        # Create a mask image for drawing purposes
        mask = np.zeros_like(old_frame)

        # OUTPUT
        width = int(cap.get(3))
        height = int(cap.get(4))
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(fileName + '_OPTICALFLOW_.avi', fourcc, 20.0, (width, height))


        while (1):
            ret, frame = cap.read()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # calculate optical flow
            p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)
            # Select good points
            good_new = p1[st == 1]
            good_old = p0[st == 1]
            # draw the tracks
            for i, (new, old) in enumerate(zip(good_new, good_old)):
                a, b = new.ravel()
                c, d = old.ravel()
                mask = cv2.line(mask, (a, b), (c, d), color[i].tolist(), 2)
                frame = cv2.circle(frame, (a, b), 5, color[i].tolist(), -1)
            img = cv2.add(frame, mask)
            out.write(img)
            if live:
                cv2.imshow('frame', img)
                k = cv2.waitKey(30) & 0xff
                if k == 27:
                    break
            # Now update the previous frame and previous points
            old_gray = frame_gray.copy()
            p0 = good_new.reshape(-1, 1, 2)
            if cap.get(cv2.CAP_PROP_POS_FRAMES) == cap.get(cv2.CAP_PROP_FRAME_COUNT):
                # If the number of captured frames is equal to the total number of frames,
                # we stop
                break
        cv2.imwrite(fileName + "_OPTICALFLOW.png", img)
        cv2.destroyAllWindows()
        cap.release()
        out.release()
        print("Optical Flow Ended")

    def stitching(self, fileName, files, mm):

        def stitch(imageA, imageB, ratio=0.75, reprojThresh=4.0,
		showMatches=False):
            def detectAndDescribe(image):
                # convert the image to grayscale
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

                # detect and extract features from the image
                descriptor = cv2.xfeatures2d.SIFT_create()
                (kps, features) = descriptor.detectAndCompute(image, None)

                # convert the keypoints from KeyPoint objects to NumPy
                # arrays
                kps = np.float32([kp.pt for kp in kps])

                # return a tuple of keypoints and features
                return kps, features

            def matchKeypoints(kpsA, kpsB, featuresA, featuresB,
                ratio, reprojThresh):
                # compute the raw matches and initialize the list of actual
                # matches
                matcher = cv2.DescriptorMatcher_create("BruteForce")
                rawMatches = matcher.knnMatch(featuresA, featuresB, 2)
                matches = []

                # loop over the raw matches
                for m in rawMatches:
                    # ensure the distance is within a certain ratio of each
                    # other (i.e. Lowe's ratio test)
                    if len(m) == 2 and m[0].distance < m[1].distance * ratio:
                        matches.append((m[0].trainIdx, m[0].queryIdx))
                # computing a homography requires at least 4 matches
                if len(matches) > 4:
                    # construct the two sets of points
                    ptsA = np.float32([kpsA[i] for (_, i) in matches])
                    ptsB = np.float32([kpsB[i] for (i, _) in matches])

                    # compute the homography between the two sets of points
                    (H, status) = cv2.findHomography(ptsA, ptsB, cv2.RANSAC,
                                                     reprojThresh)

                    # return the matches along with the homograpy matrix
                    # and status of each matched point
                    return (matches, H, status)

                # otherwise, no homograpy could be computed
                return None

            def drawMatches(imageA, imageB, kpsA, kpsB, matches, status):
                # initialize the output visualization image
                (hA, wA) = imageA.shape[:2]
                (hB, wB) = imageB.shape[:2]
                vis = np.zeros((max(hA, hB), wA + wB, 3), dtype="uint8")
                vis[0:hA, 0:wA] = imageA
                vis[0:hB, wA:] = imageB

                # loop over the matches
                for ((trainIdx, queryIdx), s) in zip(matches, status):
                    # only process the match if the keypoint was successfully
                    # matched
                    if s == 1:
                        # draw the match
                        ptA = (int(kpsA[queryIdx][0]), int(kpsA[queryIdx][1]))
                        ptB = (int(kpsB[trainIdx][0]) + wA, int(kpsB[trainIdx][1]))
                        cv2.line(vis, ptA, ptB, (0, 255, 0), 1)

                # return the visualization
                return vis

            kpsA, featuresA = detectAndDescribe(imageA)
            kpsB, featuresB = detectAndDescribe(imageB)

            # match features between the two images
            M = matchKeypoints(kpsA, kpsB,
                            featuresA, featuresB, ratio, reprojThresh)

            # if the match is None, then there aren't enough matched
            # keypoints to create a panorama
            if M is None:
                return None

            # otherwise, apply a perspective warp to stitch the images
            # together
            matches, H, status = M
            result = cv2.warpPerspective(imageA, H,
                                        (imageA.shape[1] + imageB.shape[1], imageA.shape[0]))
            result[0:imageB.shape[0], 0:imageB.shape[1]] = imageB

            # check to see if the keypoint matches should be visualized
            if showMatches:
                vis = drawMatches(imageA, imageB, kpsA, kpsB, matches,                                           status)
                # return a tuple of the stitched image and the
                # visualization
                return (result, vis)

            # return the stitched image
            return result

        print("Start Stitching")
        step = mm*100

        panorama = cv2.imread(files[0])
        for idx in range(1,len(files)-1,step):
            print("Stitching: " + str(idx) + "/" + str(int(len(files)/step)*100))
            panorama = stitch(panorama, cv2.imread(files[idx]))

        cv2.imwrite(fileName + "_STITCHING.png",panorama)
        print("Stitching Ended")


    def run(self,mm_distance):
        folderName = "tmp_"
        outVideosNames = []
        for mm in mm_distance:
            #thisFolder = folderName + str(mm)
            #if not os.path.exists(thisFolder):
            #    os.makedirs(thisFolder)

            sub = self._generateSubdividers(mm)

            files = self.files[sub]
            fileName, ext = self._ffmpg(files, mm)
            outVideosNames.append((fileName, ext))
            #self.stitching(fileName, files, mm)

        for video in outVideosNames:
            self.opticalFlow(video[0], video[1])


if __name__ == "__main__":
    v = VideoMaker("./", 25)
    v.run([1,2,4,8])
    print("Ended")