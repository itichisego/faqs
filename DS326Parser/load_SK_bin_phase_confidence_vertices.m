function s = load_SK_bin_phase_confidence_vertices(filepath)
%LOAD_SK_BIN_PHASE_CONFIDENCE_VERTICES Load phase, confidence and vertices from
%DS325 bin recording.

RESOLUTION = [240 320];

% Read the whole file as a byte array
fid = fopen(filepath);
data = fread(fid, Inf, '*uint8');
fclose(fid);

% bytes per pixel
bpp = 10;
% bytes per frame
bpf = bpp*prod(RESOLUTION);


% Reshape and split into fields
Nframes = length(data)/bpf;
data = reshape(data, [prod(RESOLUTION) bpp Nframes]);

% Extract confidence
% format is [n_rows x n_columns x n_frames], unit is digital number between 0
% and 32767
s.conf = data(:,1:2,:);
s.conf = reshape(typecast(s.conf(:), 'int16'), [flip(RESOLUTION) Nframes]);
s.conf = permute(s.conf, [2 1 3]);

% Extract phase - radial distance
% format is [n_rows x n_columns x n_frames].
% Values are int16. Negative values are reserved for flags, while positive
% values are [0, 32767], where 0 is 0 radial distance and 32767 is equal to the
% camera range which is 2998 mm.
s.phase = data(:,3:4,:);
s.phase = reshape(typecast(s.phase(:), 'int16'), [flip(RESOLUTION) Nframes]);
s.phase = permute(s.phase, [2 1 3]);

% Extract vertices - point cloud in world coordinates with the origin of the
% coordinate space in the optical center of the camera. It is a right handed
% coordinate space with x pointing to the left, y pointing down and z pointing
% in the direction of the camera optical axis.
% format is [n_rows x n_columns x n_frames x 3], where in the last dimension
% holds x,y,z coordinates in respective order.
% Units are mm.
s.vertices = data(:,5:10,:);
s.vertices = reshape(typecast(s.vertices(:), 'int16'), ...
    [3 flip(RESOLUTION) Nframes]);
s.vertices = permute(s.vertices, [3 2 4 1]);
% data is stored in left handed coordinate system - convert to right handed
s.vertices(:,:,:,2) = -s.vertices(:,:,:,2);

% Extract flags
s.flags = flags_from_phase(s.phase);

end

