function flags = flags_from_phase(phase)
%FLAGS_FROM_PHASE Extract flags from phase data.

% Flags
% low confidence - returned signal under selected threshold
flags.lowconf    = phase == -32767;
% saturation - pixel measurement saturated
flags.saturation = phase == -32766;
% aliased - pixel measurement not within unambiguous range
flags.aliased    = phase == -32765;
% kamikaze pixel 
flags.kamikaze   = phase == -32764;
% motion pixel
flags.motion     = phase == -32763;
% bad pixel
flags.badpixel   = phase == -32762;

end

