from math import sin, cos, pi, atan
import time
from utils import MovementType

from utils import DEBUG

class AcquisitionMovement:
    def __init__(self, movementType, increments, objectDistance, x, y, z):
        self.movementType = movementType
        self.increment_x = increments[0]
        self.increment_y = increments[1]
        self.increment_z = increments[2]
        self.objectDistance = objectDistance
        self.x = x["pos"]
        self.y = y["pos"]
        self.z = z["pos"]
        self.x_min = x["min"]
        self.y_min = y["min"]
        self.z_min = z["min"]
        self.x_max = x["max"]
        self.y_max = y["max"]
        self.z_max = z["max"]

        self.hypo = 0.0

        self.done = False

    def getHorizontalPosition(self, currentPosition):
        if (self.movementType == MovementType.HORIZONTAL_CONSTANT_NO_ROTATION or
                    self.movementType == MovementType.HORIZONTAL_CONSTANT_AND_ROTATION):
            self.x = currentPosition + self.increment_x
        if self.movementType == MovementType.ROTATION_CONSTANT_AND_HORIZONTAL_MOVEMENT:
            # pythagore
            if self.y_min < self.y < 90.0:
                self.hypo = self.objectDistance / sin(self.y * pi / 180.0)
                self.x = 500.0 - self.hypo * cos(self.y * pi / 180.0)
            elif self.y == 90:
                self.x = 500.0
            elif 90 < self.y < self.y_max:
                self.hypo = self.objectDistance / sin(pi - self.y * pi / 180.0)
                self.x = 500 + self.hypo * cos(pi - self.y * pi / 180.0)
            else:
                self.y = 50
        return self.x, self.hypo

    def getRotationPosition(self, currentPosition):
        if self.movementType == MovementType.HORIZONTAL_CONSTANT_NO_ROTATION:
            self.y = 90.0
        elif self.movementType == MovementType.HORIZONTAL_CONSTANT_AND_ROTATION:
            alpha = 0.0
            if self.x < 500.0:
                alpha = atan(self.objectDistance / (500.0 - self.x))
            elif self.x == 500.0:
                alpha = pi / 2.0
            else:
                alpha = pi - atan(self.objectDistance / (self.x - 500.0))

            self.y = alpha * 180.0 / pi
        elif self.movementType == MovementType.ROTATION_CONSTANT_NO_HORIZONTAL or \
                        self.movementType == MovementType.ROTATION_CONSTANT_AND_HORIZONTAL_MOVEMENT:
            self.y = currentPosition + self.increment_y
        return self.y

    def getVerticalPosition(self, currentPos):
        return currentPos + self.increment_z



class Acquisition:
    done = False
    subsequence_done = False
    movementType = False
    increments = False
    objectDistance = False
    x = False
    y = False
    z = False
    acquisitionMovement = False
    horizontalOnly = False
    checkerboard = True
    cameraHolder = False
    mail = False
    autoIncrementLine = False
    autoIncrementVertical = False
    motor = False
    hypo = 0.0

    def setCheckerboard(self, checkerboard):
        self.checkerboard = checkerboard
    def setHorizontalOnly(self, horizontalOnly):
        self.horizontalOnly = horizontalOnly
    def setMovementType(self, movement):
        self.movementType = movement
    def setDefaultIncrements(self, horizontal, vertical, rotation):
        self.increments = [horizontal, vertical, rotation]
    def setAutoIncrementLine(self, value, numberOfAcquisitionsPerLine):
        if self.increments == False:
            raise ReferenceError("Acquisition: You need to set default increments with setDefaultIncrements")
        self.autoIncrementLine = value
        if self.autoIncrementLine:
            if self.movementType == MovementType.HORIZONTAL_CONSTANT_NO_ROTATION or self.movementType == MovementType.HORIZONTAL_CONSTANT_AND_ROTATION:
                self.increments[0] = (self.x["max"] - self.x["min"])/numberOfAcquisitionsPerLine
                if DEBUG:
                    print("Horizontal increment: " + str(self.increments[0]))
            elif self.movementType == MovementType.ROTATION_CONSTANT_NO_HORIZONTAL or MovementType.ROTATION_CONSTANT_AND_HORIZONTAL_MOVEMENT:
                alpha = 0.0
                self.y["min"] = atan(self.object_distance / ((self.x["max"] - self.x["min"]) / 2.0))  # Half of total distance
                self.y["max"] = pi - self.y["min"]
                alpha = pi - 2 * self.y["min"]
                self.increments["rotation"] = (10.0 * (alpha * 180.0 / pi) / numberOfAcquisitionsPerLine) / 10.0
                self.y["min"] = self.y["min"] * 180.0 / pi
                self.y["max"] = self.y["max"] * 180.0 / pi
                if DEBUG:
                    print("rotation increment : " + str(self.rotation_increment))
    def setAutoIncrementVertical(self, value, numberOfAcquisitionsPerLine):
        self.increments[1] = -(self.z["max"] - self.z["min"]) / numberOfAcquisitionsPerLine

    def setRemainingTime(self, remainingTime):
        self.remainingTime = remainingTime

    def setObjectDistance(self, distance):
        self.objectDistance = distance
    def setX(self, pos, min, max):
        self.x = {"pos": pos, "min": min, "max": max}
    def setY(self, pos, min, max):
        self.y = {"pos": pos, "min": min, "max": max}
    def setZ(self, pos, min, max):
        self.z = {"pos": pos, "min": min, "max": max}
    def setCameraHolder(self, cameraHolder):
        self.cameraHolder = cameraHolder
    def setMail(self, mailObject):
        self.mail = mailObject
    def setMotor(self, motor):
        self.motor = motor

    def showStatus(self):
        if self.x == False or self.y == False or self.z == False:
            raise ReferenceError("Acquisition: config not set properly")
        if DEBUG: # TODO debug
            print("Positions: [" + str(self.x["pos"]) + ", " + str(self.y["pos"]) + ", " + str(self.z["pos"]) + "]")
            print("Min/max values:")
            print("x: " + str(self.x["min"]) + "/" + str(self.x["max"]))
            print("y: " + str(self.y["min"]) + "/" + str(self.y["max"]))
            print("z: " + str(self.z["min"]) + "/" + str(self.z["max"]))
            print("Object Distance: " + str(self.objectDistance))
            print("Movement type: " + str(self.movementType))
            print("increments: ")
            print("  horizontal: " + str(self.increments[0]))
            print("  vertical: " + str(self.increments[1]))
            print("  rotation: " + str(self.increments[2]))
            print("Horizontal only: " + str(self.horizontalOnly))

    def configAcquisitionMovement(self):
        #print(self.movementType)
        #if self.movementType == False:
        #    raise NotImplementedError(
        #        "Acquisition: parameter movementType not set properly")
        if self.increments == False:
            raise NotImplementedError(
                "Acquisition: parameter increments not set properly")
        if self.objectDistance == False:
            raise NotImplementedError(
                "Acquisition: parameter objectDistance not set properly")
        if self.x == False:
            raise NotImplementedError(
                "Acquisition: parameter x_pos not set properly")
        if self.y == False:
            raise NotImplementedError(
                "Acquisition: parameter y_pos not set properly")
        if self.z == False:
            raise NotImplementedError(
                "Acquisition: parameter z_pos not set properly")
        self.acquisitionMovement = AcquisitionMovement(self.movementType,
                                                  self.increments,
                                                  self.objectDistance,
                                                  self.x,
                                                  self.y,
                                                  self.z)


    def run(self):
        if self.acquisitionMovement == False:
            raise NotImplementedError(
                "Acquisition: acquisitionMovement not configured. use configAcquisitionMovement().")
        if self.cameraHolder == False:
            raise NotImplementedError(
                "Acquisition: cameraHolder not configured. use setCameraHolder().")
        if self.mail == False:
            raise NotImplementedError(
                "Acquisition: Mail not configured. use setCameraHolder().")
        if self.motor == False:
            raise NotImplementedError(
                "Acquisition: Motor not configured. use setMotor().")

        for cameraType in self.cameraHolder.keys():
            for camera in self.cameraHolder.getCameras(cameraType):
                print("Homing..")
                if not DEBUG:
                    self.motor.homing()
                    # TODO publisher
                    self.x["pos"] = self.motor.x
                    self.y["pos"] = self.motor.y
                    self.z["pos"] = self.motor.z
                print("Homing finished")

                print("Acquisition running")
                while not self.done:
                    while not self.subsequence_done:
                        self.x["pos"], self.hypo = self.acquisitionMovement.getHorizontalPosition(self.x["pos"])
                        self.y["pos"] = self.acquisitionMovement.getRotationPosition(self.y["pos"])

                        # check if less than min
                        if (self.x["pos"] < self.x["min"]):
                            self.x["pos"] = self.x["min"]
                        if (self.y["pos"] < self.y["min"]):
                            self.y["pos"] = self.y["min"]
                        if (self.z["pos"] < self.z["min"]):
                            self.z["pos"] = self.z["min"]

                        # check if more than max
                        if (self.x["pos"] > self.x["max"]):
                            self.x["pos"] = self.x["max"]
                            self.subsequence_done = True
                        if (self.y["pos"] > self.y["max"]):
                            self.y["pos"] = self.y["max"]
                        if (self.z["pos"] > self.z["max"]):
                            self.z["pos"] = self.z["max"]

                        self.remainingTime.getRemainingTime(self.x["pos"])
                        camera.takePhoto(str(self.z["pos"]))

                        if not self.subsequence_done:
                            self.showStatus()

                            # send command to motors
                            if not DEBUG:
                                self.motor._moveToPosition(self.x["pos"], self.y["pos"], self.z["pos"])

                            if self.checkerboard:
                                pass # TODO
                            else:
                                pass # TODO

                            # move vertically
                            pos = self.x["pos"]
                            max = self.x["max"]
                            #if (self.movementType == MovementType.ROTATION_CONSTANT_AND_HORIZONTAL_MOVEMENT or
                            #    self.movementType == MovementType.ROTATION_CONSTANT_NO_HORIZONTAL):
                            #    pos = self.y["pos"]
                            #    max = self.y["max"]
                            if (pos >= max):
                                camera.takePhoto(str(self.z["pos"]))
                                print("End of line " + time.strftime("%A %d %B %Y %H:%M:%S"))
                                self.subsequence_done = True
                    camera.photoNumber = 1
                    print("Subsequence done " + time.strftime("%A %d %B %Y %H:%M:%S"))
                    self.mail.send("Subsequence finished. " + time.strftime("%A %d %B %Y %H:%M:%S") + ". Height:" + str(self.z["pos"]))
                    if self.z["pos"] <= self.z["max"] - self.acquisitionMovement.increment_z:
#                        continue_var = input(" Continue ? y/n")
#                        if continue_var == "n":
#                           self.done = True
#                        else:
                            self.x["pos"] = self.x["min"]
                            self.y["pos"] = self.y["min"]
                            self.z["pos"] = self.acquisitionMovement.getVerticalPosition(self.z["pos"])
                            if not DEBUG: # TODO HOMING ENTRE SUBSEQUENCES ?
                                #self.motor.homing()
                                self.motor._moveToPosition(self.x["pos"], self.y["pos"], self.z["pos"])
                                time.sleep(10) # TODO important car sinon il va faire les photos en montant # TODO trouver une bonne valeur..
                            self.subsequence_done = False
                            #checkerboard_var = input(" Checkerboard ? y/n")
                            #if checkerboard_var == "n":
                            #    self.checkerboard = False
                            #    self.subsequence_done = False
                            #    # TODO publisher
                            #    self.x["pos"] = self.x["min"]
                            #    if not self.horizontalOnly:
                            #        self.z["pos"] = self.z["min"]
                            #        self.motor._moveToPosition(self.x["pos"], self.y["pos"], self.z["pos"])
                            #        self.mail.send("Acquisition finished. " + time.strftime("%A %d %B %Y %H:%M:%S"))
                    else:
                        self.done = True
                        # checkerboard_var = input(" Checkerboard ? y/n")
                        # if checkerboard_var == "n":
                        #    self.checkerboard = False
                        #    self.subsequence_done = False
                        #    # TODO publisher
                        #    self.x["pos"] = self.x["min"]
                        #    if not self.horizontalOnly:
                        #        self.z["pos"] = self.z["min"]
                        #        self.motor._moveToPosition(self.x["pos"], self.y["pos"], self.z["pos"])
                        #        self.mail.send("Acquisition finished. " + time.strftime("%A %d %B %Y %H:%M:%S"))

                self.mail.send(
                    "SEQUENCE finished. " + time.strftime("%A %d %B %Y %H:%M:%S") + ".")
            #camera.cleanUp()