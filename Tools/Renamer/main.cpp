#include <iostream>
#include <fstream>
#include <string>

#include "dirent.h"

#include "configuration.h"

char buf[100];

bool isFormat(std::string nam, std::string ext)
{
    int siz = int(nam.size() - ext.size());
    int pos = int(nam.rfind(ext));
    return (pos == siz);
}
bool isReadable(const std::string nam)
{
    std::ifstream fil(nam.c_str());
    return !fil.fail();
}
bool isDirectory(const std::string nam)
{
    std::string fil = nam;
    int pos = int(fil.rfind("*"));
    if(pos != std::string::npos)
        fil = fil.substr(0, pos);

    if(fil.size() == 0)
        fil = ".";

    DIR *rep = opendir(fil.c_str());
    if(rep != NULL)
    {
        closedir(rep);
        return true;
    }
    return false;
}
void getDirectoryFilename(const std::string nam, std::vector<std::string> &str_fil, bool sub_dir = false)
{
    std::string fil = nam;
    std::string ext = "";

    int pos = int(fil.rfind("*"));
    if(pos != std::string::npos)
    {
        ext = fil.substr(pos + 1);
        fil = fil.substr(0, pos);
    }

    if(fil[int(fil.size()) - 1] != '\\')
        fil += '\\';

    DIR *rep = opendir(fil.c_str());
    if(rep != NULL)
    {
        struct dirent *ent;
        while((ent = readdir(rep)) != NULL)
        {
            std::string inp = fil + ent->d_name;
            if(inp[inp.size() - 1] == '.')
                continue;

            if(isDirectory(inp))
            {
                if(sub_dir) str_fil.push_back(inp);
            }
            else
            {
                if(isFormat(inp, ext)) str_fil.push_back(inp);
            }
        }
        closedir(rep);
    }
}

std::string getFilename(std::string nam)
{
    int pos = int(nam.rfind("."));
    if(pos != std::string::npos)
        nam = nam.substr(0, pos);

    pos = int(nam.rfind("\\"));

    if(pos != std::string::npos)
        nam = nam.substr(pos + 1);

    return nam;
}

void decomposePathname(std::string pth, std::string &dir, std::string &nam, std::string &ext)
{
    dir = "";
    nam = "";
    ext = "";

    int pos = int(pth.rfind("."));
    if(pos != std::string::npos)
    {
        ext = pth.substr(pos);
        pth = pth.substr(0, pos);
    }

    pos = int(pth.rfind("\\"));
    if(pos != std::string::npos)
    {
        nam = pth.substr(pos + 1);
        dir = pth.substr(0, pos + 1);
    }
}
#include <stdexcept>
std::string changeNumber(std::string fil, int idx_max)
{
    int siz = int(fil.size());
    std::string nam = fil.substr(0, siz - 4) + "%04d";
    std::string num = fil.substr(siz - 4);

    std::string out = "";
    try{
        int idx = -1;
        idx = std::stoi(num);

        char buf[1000];
        sprintf(buf, nam.c_str(), idx_max - idx + 1);

        out = std::string(buf);
    }
    catch(std::invalid_argument&){
        out = fil;
        std::cout << "Filename unchanged: " << out << std::endl;
    }

    return out;
}

int main(int argc, char** argv)
{
    Configuration cfg;
    cfg.load("Config_Correction.cfg");

    bool sub_dir = false;
    int idx_max = 0;

    cfg.get("sub_dir", sub_dir);
    cfg.get("idx_max", idx_max);





    std::vector<std::string> str_fil;
    if(argc == 1)
        str_fil.push_back(".");

    for(int i = 1 ; i < argc ; i++)
        str_fil.push_back(argv[i]);   

    std::vector<std::string> ftr_fil;
    for(int i = 0 ; i < int(str_fil.size()) ; i++) {
        if(isDirectory(str_fil[i])) {
            std::cout << "Readed folder: " << str_fil[i] << std::endl;
            getDirectoryFilename(str_fil[i], str_fil, sub_dir);
            continue;
        }
        else
        {
            if(isFormat(str_fil[i], ".png"))
                ftr_fil.push_back(str_fil[i]);
        }
    }




    std::string add_ext = "_RNM";
    std::vector<std::string> ftr_dir(ftr_fil.size());
    std::vector<std::string> ftr_nam(ftr_fil.size());
    std::vector<std::string> ftr_ext(ftr_fil.size());
    std::vector<std::string> ftr_mod(ftr_fil.size());

    for(int i = 0 ; i < int(ftr_fil.size()) ; i++)
    {
        decomposePathname(ftr_fil[i], ftr_dir[i], ftr_nam[i], ftr_ext[i]);
        ftr_mod[i] = changeNumber(ftr_nam[i], idx_max);
    }



    for(int i = 0 ; i < int(ftr_fil.size()) ; i++)
    {
        std::string cmd = "rename ";
        cmd += "\"";
        cmd += ftr_fil[i];
        cmd += "\" \"";
        cmd += ftr_mod[i];
        cmd += add_ext;
        cmd += ftr_ext[i];
        cmd += "\"";

        std::system(cmd.c_str());
    }
    for(int i = 0 ; i < int(ftr_fil.size()) ; i++)
    {
        std::string cmd = "rename ";
        cmd += "\"";
        cmd += ftr_dir[i];
        cmd += ftr_mod[i];
        cmd += add_ext;
        cmd += ftr_ext[i];
        cmd += "\" \"";
        cmd += ftr_mod[i];
        cmd += ftr_ext[i];
        cmd += "\"";

        std::system(cmd.c_str());
    }

    return 0;
}

