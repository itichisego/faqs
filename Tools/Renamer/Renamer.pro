TEMPLATE = app
CONFIG += console
CONFIG -= qt


contains(QT_ARCH, x86_64)|contains(QMAKE_HOST.arch, x86_64) {
    #message(Compiling 64-bit program)
    #win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LibPointScan/ -lPointScan64
    #else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LibPointScan/ -lPointScan64d
    LIBS += -L$${OPENCVPATH}/x64/vc12/lib
} else {
    #message(Compiling 32-bit program)
    #win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LibPointScan/ -lPointScan32
    #else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LibPointScan/ -lPointScan32d
    LIBS += -L$${OPENCVPATH}/x86/vc12/lib
}

QMAKE_CXXFLAGS += -openmp


SOURCES += main.cpp
SOURCES += configuration.cpp

HEADERS += configuration.h \
           dirent.h


TARGET = Renamer
DESTDIR = ../Renamer
