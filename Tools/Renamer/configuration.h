#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#ifdef __cplusplus

#include <map>
#include <string>
#include <vector>

class Configuration
{
    public:
        //Clear all values
        void clear();

        //Load a configuration file
        bool load(const std::string& fil);

        //Check if value associated with given key exists
        bool contains(const std::string& key) const;

        //Get value associated with given key
        bool get(const std::string& key, std::string& val) const;
        bool get(const std::string& key, bool&    val) const;
        bool get(const std::string& key, double&  val) const;
        bool get(const std::string& key, float&   val) const;
        bool get(const std::string& key, int&     val) const;
        bool get(const std::string& key, long&    val) const;

        bool get(const std::string& key, double   val[], int siz = 16) const;
        bool get(const std::string& key, float    val[], int siz = 16) const;

        bool get(const std::string& key, std::vector<int> &val) const;

    private:
        //Data container
        std::map<std::string, std::string> cnt;

        //Remove leading and trailing tabs and spaces
        static std::string trim(const std::string& str);
};

#endif // __cplusplus

#endif // CONFIGURATION_H


