#include "configuration.h"

#include <fstream>

void Configuration::clear()
{
    cnt.clear();
}
bool Configuration::load(const std::string& fil)
{
    std::ifstream inf(fil.c_str());

    if(!inf.good())
    {
        //std::cout << "Cannot read configuration file " << fil << std::endl;
        return false;
    }
    while(inf.good() && ! inf.eof())
    {
        std::string lin;
        getline(inf, lin);

        // filter out comments
        if(!lin.empty())
        {
            int pos = int(lin.find('#'));
            if(pos != std::string::npos)
            {
                lin = lin.substr(0, pos);
            }
        }

        // split line into key and value
        if(!lin.empty())
        {
            int pos = int(lin.find('='));
            if(pos != std::string::npos)
            {
                std::string key = trim(lin.substr(0, pos));
                std::string val = trim(lin.substr(pos + 1));
                if(!key.empty() && !val.empty())
                {
                    cnt[key] = val;
                }
            }
        }
    }
    return true;
}

bool Configuration::contains(const std::string& key) const
{
    return cnt.find(key) != cnt.end();
}

bool Configuration::get(const std::string& key, bool&   val) const
{
    std::string str;
    if(get(key, str))
    {
        val =(str == "true");
        return true;
    }
    else
    {
        return false;
    }
}
bool Configuration::get(const std::string& key, double& val) const
{
    std::string str;
    if(get(key, str))
    {
        val = atof(str.c_str());
        return true;
    }
    else
    {
        return false;
    }
}
bool Configuration::get(const std::string& key, float&  val) const
{
    std::string str;
    if(get(key, str))
    {
        val = (float)atof(str.c_str());
        return true;
    }
    else
    {
        return false;
    }
}
bool Configuration::get(const std::string& key, int&    val) const
{
    std::string str;
    if(get(key, str))
    {
        val = atoi(str.c_str());
        return true;
    }
    else
    {
        return false;
    }
}
bool Configuration::get(const std::string& key, long&   val) const
{
    std::string str;
    if(get(key, str))
    {
        val = atol(str.c_str());
        return true;
    }
    else
    {
        return false;
    }
}
bool Configuration::get(const std::string& key, std::string& val) const
{
    std::map<std::string, std::string>::const_iterator itr = cnt.find(key);

    if(itr != cnt.end())
    {
        val = itr->second;
        return true;
    }
    else
    {
        return false;
    }
}

bool Configuration::get(const std::string& key, double  val[], int siz) const
{
    std::string str;
    int itr = 0;
    if(get(key, str))
    {
        while(!str.empty())
        {
            int pos = int(str.find(' '));
            if(pos != std::string::npos)
            {
                std::string dat = trim(str.substr(0, pos));
                str = trim(str.substr(pos + 1));

                if(itr < siz)
                {
                    val[itr] = atof(dat.c_str());
                }
            }
            else
            {
                if(itr < siz)
                {
                    val[itr] = atof(str.c_str());
                }
                str.clear();
            }
            itr++;
        }
    }

    if(itr == siz)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool Configuration::get(const std::string& key, float   val[], int siz) const
{
    std::string str;
    int itr = 0;
    if(get(key, str))
    {
        while(!str.empty())
        {
            int pos = int(str.find(' '));
            if(pos != std::string::npos)
            {
                std::string dat = trim(str.substr(0, pos));
                str = trim(str.substr(pos + 1));

                if(itr < siz)
                {
                    val[itr] = (float)atof(dat.c_str());
                }
            }
            else
            {
                if(itr < siz)
                {
                    val[itr] = (float)atof(str.c_str());
                }
                str.clear();
            }
            itr++;
        }
    }

    if(itr == siz)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Configuration::get(const std::string& key, std::vector<int> &val) const
{
    std::string str;
    if(get(key, str))
    {
        while(!str.empty())
        {
            int pos = int(str.find(' '));
            if(pos != std::string::npos)
            {
                std::string dat = trim(str.substr(0, pos));
                str = trim(str.substr(pos + 1));

                val.push_back(atoi(dat.c_str()));
            }
            else
            {
                val.push_back(atoi(str.c_str()));
                str.clear();
            }


        }
        return true;
    }

    return false;
}

std::string Configuration::trim(const std::string& str)
{
    int fst = int(str.find_first_not_of(" \t"));
    if(fst != std::string::npos)
    {
        int lst = int(str.find_last_not_of(" \t"));
        return str.substr(fst, lst - fst + 1);
    }
    else
    {
        return "";
    }
}
