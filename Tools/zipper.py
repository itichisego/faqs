
import os
import zipfile

'''
WARNING:
Put this file in root directory
/path/dir/textures/
/path/dir/masks/
/path/dir/depth/
/path/dir/depth_mapped/

put this file in /path/dir/.
'''

def zipFiles(path = '.', filter = 'yuv', subName = ""):
    '''
    :param path: path of the rootdir (should be always '.')
    :param filter: filter by "endname" ! filter in fileName, if the filename contains sometyuving.png, it will be zipped!
    :param subName: text to append at the end of the zip file typeOfZip_lineNumbers"subname".zip, eg: "_png"
    :return: None
    '''
    walk = os.walk(path)
    for w in walk:
        if w[1] == []: # only if there is no more subdirs
            currentPath = w[0] # base directory
            currentPathSplit = currentPath.split('\\')
            if len(currentPathSplit) == 3:
                dirName = currentPathSplit[1]
                lineName = currentPathSplit[2]
                allPaths = []
                for fileName in w[2]: # all files in the directory
                    if filter in fileName: # eg: check if "png" is in fileName
                        allPaths.append(currentPath + "\\" + fileName)

                if allPaths != []:
                    zip = zipfile.ZipFile(dirName + "_" + lineName + subName + ".zip", "w")
                    print("Zipping: " + dirName + "/" + lineName)
                    for file in allPaths:
                        zip.write(file, arcname=os.path.basename(file), compress_type=zipfile.ZIP_DEFLATED)


def mkMOCKTree(path, numberOfDirs, numberOfFiles):
    def makeMOCKFiles(filePath):
        for idx in range(numberOfFiles):
            open(filePath + "/" + str(idx) + ".yuv", 'a').close()
            open(filePath + "/" + str(idx) + ".png", 'a').close()
            open(filePath + "/" + str(idx) + ".other", 'a').close()

    for idx in range(numberOfDirs):
        tmpPath = path + "/texture/" + str(idx) + ".0"
        os.makedirs(tmpPath)
        makeMOCKFiles(tmpPath)
        tmpPath = path + "/depth/" + str(idx) + ".0"
        os.makedirs(tmpPath)
        makeMOCKFiles(tmpPath)
        tmpPath = path + "/mapped/" + str(idx) + ".0"
        os.makedirs(tmpPath)
        makeMOCKFiles(tmpPath)
        tmpPath = path + "/test/" + str(idx) + ".0"
        os.makedirs(tmpPath)
        makeMOCKFiles(tmpPath)


if __name__ == "__main__":
    #mkMOCKTree(".", 10, 10) # DEBUG ONLY
    # zipFiles(".", "png", "_png")
    zipFiles(".", "yuv")
    # zipFiles(".", "other", "_other")
