from FAQSv2.AcquisitionController import Acquisition

from FAQSv2.config import ORDER

destination_folder = "H:/TEST_K_LIGNE"

# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("PLANLigneX200400_Y_Ziiii0")
# a.setDescription("Test dataset with all z, y=[200, 10, 411] and all x")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([0, 1, 3], [30, 1, 32], [0, 1, 2], (3, 2, 1))
#
# a.doAcquisition()
# a.close()


# # End plane

# # TIM
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("TIM_100x100x100_by_10_2")
# a.setDescription("cube Tim")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([400, 10, 501], [350, 10, 451], [100, 10, 201], ORDER["HORIZONTAL_VERTICAL_DEPTH"])
#
# a.doAcquisition()
# a.close()

# # CUBE
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("FOUR_PLANS")
# a.setDescription("Four plans")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([275, 1, 601], [300, 1, 451], [50, 250, 560], ORDER["HORIZONTAL_VERTICAL_DEPTH"])
#
# a.doAcquisition()
# a.close()

# # CUBE2
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("FOUR_PLANS")
# a.setDescription("Four plans")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([275, 1, 601], [360, 1, 451], [550, 250, 560], ORDER["HORIZONTAL_VERTICAL_DEPTH"])
#
# a.doAcquisition()
# a.close()

# # Z plans
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("Z_PLANS")
# a.setDescription("Two plans dense acquisition")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([407, 60, 470], [300, 1, 451], [50, 1, 551], ORDER["VERTICAL_DEPTH_HORIZONTAL"])
#
# a.doAcquisition()
# a.close()

# # Z plans 407
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination("I:/DATASET")
# a.setName("Z_PLANS407")
# a.setDescription("Two plans dense acquisition")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([407, 60, 420], [300, 1, 451], [50, 1, 551], ORDER["VERTICAL_DEPTH_HORIZONTAL"])
#
# a.doAcquisition()
# a.close()
#
# # Z plans 467
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination("I:/DATASET")
# a.setName("Z_PLANS467")
# a.setDescription("Two plans dense acquisition")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([467, 60, 470], [300, 1, 451], [50, 1, 551], ORDER["VERTICAL_DEPTH_HORIZONTAL"])
#
# a.doAcquisition()
# a.close()

# # Z plans RECUP
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("Z_PLANS_2")
# a.setDescription("Two plans dense acquisition")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([467, 60, 470], [399, 1, 451], [50, 1, 551], ORDER["VERTICAL_DEPTH_HORIZONTAL"])
#
# a.doAcquisition()
# a.close()

# # Z plans RECUP2
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("Z_PLANS_3")
# a.setDescription("Two plans dense acquisition")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([407, 60, 410], [399, 1, 451], [50, 1, 551], ORDER["VERTICAL_DEPTH_HORIZONTAL"])
#
# a.doAcquisition()
# a.close()


# # Z plans 467 complete
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("Z_PLANS_467")
# a.setDescription("Two plans dense acquisition")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([467, 60, 470], [300, 1, 451], [50, 1, 551], ORDER["VERTICAL_DEPTH_HORIZONTAL"])
#
# a.doAcquisition()
# a.close()


# # DENSE Z
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("FOUR_PLANS_DENSE")
# a.setDescription("Four plans dense acquisition")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([275, 1, 601], [300, 1, 451], [50, 1, 60], ORDER["HORIZONTAL_VERTICAL_DEPTH"])
#
# a.doAcquisition()
# a.close()


from FAQSv2.flatHeightGenerator import flatHeightGenerator
points_eye = []
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
for eye in range(0,2):
    # EIGHT EYEx
    a = Acquisition(show_command=False, marlin_log=False)

    a.setVersion(1)
    a.setDestination(destination_folder)
    a.setName("EIGHT EYEi_{}".format(str(eye)))
    a.setDescription("Eight Eye{} dataset".format(str(eye)))
    a.setCamera("Kinect")

    points_eye.append({
        "destination_folder": destination_folder + "/EIGHT EYEi_{}".format(str(eye))
    })

    a.setZip(False)

    # NORMAL X Y Z AXIS SYSTEM
    points, x, y, z = flatHeightGenerator(lim_x=(275,601), lim_y=(300,451), lim_z=(0,550), eye_number=eye, images_number=800)
    #points, x, y, z = flatHeightGenerator(lim_x=(0,100), lim_y=(0,100), lim_z=(0,100), eye_number=eye, eyes_dist=5, images_number=100)
    points_eye[eye]["points"] = points
    points_eye[eye]["points_scat"] = (x, y, z)
    a.addPoints(x, y, z)

    a.doAcquisition()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x, y, z, label='parametric curve')
    ax.scatter(x, y, z, label='scatter curve')
    plt.savefig("path_{}.png".format(str(eye))) #points_eye[0]["destination_folder"] + "/path_{}.png".format(str(eye)))
    a.close()

# SAVING GRAPHS

# gen lines between eyes
eyes_links = list(zip(points_eye[0]["points"], points_eye[1]["points"]))
lines_x = [[line[0][0], line[1][0]] for line in eyes_links]
lines_y = [[line[0][1], line[1][1]] for line in eyes_links]
lines_z = [[line[0][2], line[1][2]] for line in eyes_links]

points = points_eye[0]["points"]
points.extend(points_eye[1]["points"])
x = points_eye[0]["points_scat"][0]
x.extend(points_eye[1]["points_scat"][0])
y = points_eye[0]["points_scat"][1]
y.extend(points_eye[1]["points_scat"][1])
z = points_eye[0]["points_scat"][2]
z.extend(points_eye[1]["points_scat"][2])


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(x, y, z, label='parametric curve')
ax.scatter(x, y, z, label='scatter curve')
for i in range(len(lines_x)):
    ax.plot(lines_x[i], lines_y[i], lines_z[i], color='r')
plt.savefig("pathEyes.png")#points_eye[0]["destination_folder"] + "/pathEyes.png")
plt.savefig(points_eye[1]["destination_folder"] + "/pathEyes.png")

# STEP OUT
a = Acquisition(show_command=False, marlin_log=False)

a.setVersion(1)
a.setDestination("H:/DATASET")
a.setName("STEP OUT")
a.setDescription("Test dataset")
a.setCamera("Kinect")

a.setZip(False)

# NORMAL X Y Z AXIS SYSTEM
#a.addBlock([435, 1, 441], [320, 1, 326], [0, 1, 550], ORDER["HORIZONTAL_VERTICAL_DEPTH"])
a.addBlock([495, 1, 501], [320, 1, 326], [0, 1, 550], ORDER["HORIZONTAL_VERTICAL_DEPTH"])

a.addBlock([435, 1, 441], [340, 1, 346], [0, 1, 550], ORDER["HORIZONTAL_VERTICAL_DEPTH"])
a.addBlock([495, 1, 501], [340, 1, 346], [0, 1, 550], ORDER["HORIZONTAL_VERTICAL_DEPTH"])

a.doAcquisition()
a.close()


# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("PLANLigneX200400_Y_Z0")
# a.setDescription("Test dataset with z = 0, y=[200, 10, 411] and all x")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([0, 1, 800], [200, 10, 411], [0, 1, 1], (3, 2, 1))
#
# a.doAcquisition()
# a.close()

# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("testLigneX_Y220_Z200")
# a.setDescription("Test dataset with z=200, y=220 and all x")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([0, 1, 870], [220, 1, 221], [200, 10, 201], (2, 3, 1))
#
# a.doAcquisition()
# a.close()
#
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("testLigneX_Y220_Z400")
# a.setDescription("Test dataset with z=400, y=220 and all x")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([0, 1, 870], [220, 1, 221], [400, 10, 401], (3, 2, 1))
#
# a.doAcquisition()
# a.close()
#
# a = Acquisition(show_command=False, marlin_log=False)
#
# a.setVersion(1)
# a.setDestination(destination_folder)
# a.setName("testLigneX_Y220_Z600")
# a.setDescription("Test dataset with z=600, y=220 and all x")
# a.setCamera("Kinect")
#
# a.setZip(False)
#
# # NORMAL X Y Z AXIS SYSTEM
# a.addBlock([0, 1, 870], [220, 1, 221], [600, 1, 601], (1, 2, 3))
#
# a.doAcquisition()
# a.close