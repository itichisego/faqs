# FAQS - Fast AcQuisition System

# make folders
import os
import time

from utils import MovementType, Mail
from acquisition import Acquisition
from cameras import CamerasHolder, RGB, DS326, Kinect
from motor import Motor
from remainingTime import RemainingTime

from utils import DEBUG

if __name__ == "__main__":

    # DEBUG VARIABLE SET IN utils.py

    mail = Mail("lisa.ulb.camera", "LisaCam3ra", "lisa.ulb.camera@gmail.com", "lisa.ulb.camera@gmail.com")

    RGB_flag = False
    DS326_flag = False
    kinect_flag = True

    if RGB_flag + DS326_flag + kinect_flag > 1:
        exit()

    x = 0.0
    x_min = 150.0
    x_max = 1000.0 #250.0
    y = 0.0
    y_pos = 10.0
    y_min = 10.0
    y_max = 10.0
    z = 1000.0
    z_min = 564.0 # -27.0 for kinect and sk
    z_max = 564.0 # -27.0 for kinect and sk

    if z_min > z_max:
        z_min, z_max = z_max, z_min

    if kinect_flag or DS326_flag:
        z_min -= 27.0
        z_max -= 27.0

    horizontal = vertical = 1.0
    rotation = 0.0

    remainingTime = RemainingTime(x_max, horizontal)

    def addMultipleCamera(camHolder, cameraObj, number, numberPhotos):
        for idx in range(number):
            cam = cameraObj
            camHolder.add()

    camHolder = CamerasHolder()
    if RGB_flag:
        camHolder.add(RGB, photoNumber=1, softwarePath="C:\\Program Files (x86)\\digiCamControl\\")
    if kinect_flag:
        camHolder.add(Kinect, photoNumber=1, softwarePath="C:\\Program Files\\Microsoft SDKs\\Kinect\\v2.0_1409\\Tools\\KinectStudio\\")
    if DS326_flag:
        camHolder.add(DS326, photoNumber=1, softwarePath="C:\\Program Files\\SoftKinetic\\DepthSenseSDK\\bin\\")
    camHolder.showCameras()

    motor = Motor()
    motor.setCenterX(560) # TODO set the right one
    motor.setX(x_min, x_max)
    motor.setY(y_min, y_max)
    motor.setZ(z_min, z_max)

    motor.setOnlyX() # TODO remove if unwanted

    #motor.setWaitTimeBetweenMovements(1)

    acquisition = Acquisition()
    acquisition.setMail(mail)
    acquisition.setRemainingTime(remainingTime)
    acquisition.setCameraHolder(camHolder)
    acquisition.setCheckerboard(True)
    acquisition.setHorizontalOnly(False)
    acquisition.setDefaultIncrements(horizontal, rotation, vertical)
    #acquisition.setAutoIncrementLine(True, numberOfAcquisitionsPerLine=10)
    #acquisition.setAutoIncrementVertical(True, numberOfAcquisitionsPerLine=10)
    acquisition.setMovementType(MovementType.HORIZONTAL_CONSTANT_NO_ROTATION)
    acquisition.setObjectDistance(640) # TODO ???
    acquisition.setX(pos=x, min=x_min, max=x_max)
    acquisition.setY(pos=y, min=y_min, max=y_max)
    acquisition.setZ(pos=z, min=z_min, max=z_max)

    acquisition.configAcquisitionMovement()
    acquisition.setMotor(motor)

    # start logging TODO (encoder_app/main.py)
    acquisition.run()

