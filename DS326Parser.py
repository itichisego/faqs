import numpy as np
import cv2

def flags_from_phase(phase):
    flags = {}
    # low confidence - Returned signal under selected threshold
    flags["lowconf"] = phase == -32767
    # saturation - pixel measurement satured
    flags["saturation"] = phase == -32766
    # aliased - Pixel measurement not within unambiguous range
    flags["aliased"] = phase == -32765
    # kamikaze pixel
    flags["kamikaze"] = phase == -32764
    # motion pixel
    flags["motion"] = phase == -32763
    # bad pixel
    flags["badpixel"] = phase == -32762

    return flags

def load_SK_bin_phase_confidence_vertices(filepath):
    RESOLUTION = {}
    RESOLUTION["horizontal"] = 320
    RESOLUTION["vertical"] = 240
    RESOLUTION["frameSize"] = RESOLUTION["vertical"] * RESOLUTION["horizontal"]

    bytesPerPixel = 10
    bytesPerFrame = bytesPerPixel * RESOLUTION["frameSize"]

    with open(filepath, "rb") as fid:
        data = np.fromfile(fid, np.uint8)
        NFrames = np.size(data)/bytesPerFrame
        data = np.reshape(data, [RESOLUTION["frameSize"], NFrames, bytesPerPixel], order="F").copy()

        # Extract confidence
        # format is [n_rows x n_columns x n_frames], unit is digital number between 0
        # and 32767

        confView = np.copy(data[:, :, 0:2])
        confv = np.reshape(confView, [np.size(confView)], order="F") # array 1D
        conf = confv.view(np.int16)
        conf = np.reshape(conf, [RESOLUTION["horizontal"], RESOLUTION["vertical"], NFrames], order="F").copy()
        conf = np.transpose(conf, [1, 0, 2])

        # Extract phase - radial distance
        # format is [n_rows x n_columns x n_frames].
        # Values are int16. Negative values are reserved for flags, while positive
        # values are [0, 32767], where 0 is 0 radial distance and 32767 is equal to the
        # camera range which is 2998 mm.

        phaseView = np.copy(data[:, :, 2:4])
        phasev = np.reshape(phaseView, [np.size(phaseView)], order="F")  # array 1D
        phase = phasev.view(np.int16)
        phase = np.reshape(phase, [RESOLUTION["horizontal"], RESOLUTION["vertical"], NFrames], order="F").copy()
        phase = np.transpose(phase, [1, 0, 2])

        # Extract vertices - point cloud in world coordinates with the origin of the
        # coordinate space in the optical center of the camera. It is a right handed
        # coordinate space with x pointing to the left, y pointing down and z pointing
        # in the direction of the camera optical axis.
        # format is [n_rows x n_columns x n_frames x 3], where in the last dimension
        # holds x,y,z coordinates in respective order.
        # Units are mm.

        verticesView = np.copy(data[:, :, 4:10])
        verticesv = np.reshape(verticesView, [np.size(verticesView)], order="F")  # array 1D
        vertices = verticesv.view(np.int16)
        vertices = np.reshape(vertices, [3, RESOLUTION["horizontal"], RESOLUTION["vertical"], NFrames], order="F").copy()
        #print(np.size(vertices, 0), np.size(vertices, 1), np.size(vertices, 2), np.size(vertices, 3))
        vertices = np.transpose(vertices, [2, 1, 3, 0])
        #print(np.size(vertices, 0), np.size(vertices, 1), np.size(vertices, 2), np.size(vertices, 3))
        vertices[:, :, :, 2] = -vertices[:, :, :, 2]

        flags = flags_from_phase(phase)

        values = {}
        values["conf"] = conf
        values["phase"] = phase
        values["vertices"] = vertices
        values["flags"] = flags

        return values

def save_image(fileName, val):
    RESOLUTION = {}
    RESOLUTION["horizontal"] = 320
    RESOLUTION["vertical"] = 240
    nbrAcq = np.size(val, 2)

    # val est en int16 il faut la sauver en uint16 ET en uint8 et ajouter ".png"
    tmp = np.zeros([RESOLUTION["vertical"], RESOLUTION["horizontal"]], dtype=np.uint64)
    for idx in range(1, nbrAcq):
        #tmp[:, :] = tmp[:, :] + 32768
        tmp[:, :] = tmp[:, :] + np.copy(val[:,:,idx])
    tmp = tmp[:, :]/(nbrAcq - 1)

    # uint6
    img16 = np.uint16(np.copy(tmp))
    cv2.imwrite(fileName + "_uint16.png", img16)

    # uint8
    #minmax = cv2.minMaxLoc(img16)
    #img8 = img16 / minmax[1]
    #img8 = img8 * 255
    #img8 = np.uint8(255 - np.copy(img16) / 2**8) #TODO check si regle de trois mieux que minmax
    #cv2.imwrite(fileName + "_uint8.png", img8)
    saveUint8Image(fileName + "_uint16.png", fileName + "_uint8.png")

def saveUint8Image(fileName, out):
    image = cv2.imread(fileName, flags=cv2.IMREAD_ANYDEPTH)
    minmax = cv2.minMaxLoc(image)

    min_depth = minmax[1] / 8.0
    max_depth = minmax[1] / 6.0

    img = np.empty(image.shape, 'uint8')
    alpha = 255.0 / (max_depth - min_depth)
    beta = -alpha * min_depth
    cv2.convertScaleAbs(image, img, alpha, beta)
    cv2.imwrite(out, img)

def save_PointCloud(filename, val):
    print("point cloud dims", str(np.size(val,0)), str(np.size(val,1)), str(np.size(val,2)), str(np.size(val,3)))

    # save a ply file

    numX = np.size(val, 0)
    numY = np.size(val, 1)
    numScans = np.size(val, 2)

    total = []
    count = 0
    maxPos = -10000
    minPos = 10000
    for idx in range(numX):
        for idy in range(numY):
            for scan in range(1,numScans): # TODO remove the one
                ptx = val[idx, idy, scan, 0]
                pty = val[idx, idy, scan, 1]
                ptz = val[idx, idy, scan, 2]

                if abs(ptx) < 3000 and abs(pty) < 3000 and abs(ptz) < 2100:
                    if (ptx > maxPos):
                        maxPos = ptx
                    if (pty > maxPos):
                        maxPos = pty
                    if (ptz > maxPos):
                        maxPos = ptz
                    if (ptx < minPos):
                        minPos = ptx
                    if (pty < minPos):
                        minPos = pty
                    if (ptz < minPos):
                        minPos = ptz
                    total.append([ptx, pty, ptz])
                    count = count + 1

    header = "ply\n" \
    "format ascii 1.0\n" \
    "comment made for 3DLicorneA\n" \
    "element vertex " + str(count) + "\n" \
    "property float x\n" \
    "property float y\n" \
    "property float z\n" \
    "property uchar red\n" \
    "property uchar blue\n" \
    "property uchar green\n" \
    "end_header\n"

    with open(filename + ".ply", "w") as f:
        f.write(header)
        for idx in range(count):
            f.write(str(total[idx][0]) + " " \
                    + str(total[idx][1]) + " " \
                    + str(total[idx][2]) + " " \
                    + str(np.ubyte(255*((total[idx][0]+abs(minPos))/(maxPos + abs(minPos))))) + " " \
                    + str(np.ubyte(255*((total[idx][1]+abs(minPos))/(maxPos + abs(minPos))))) + " " \
                    + str(np.ubyte(255*((total[idx][2]+abs(minPos))/(maxPos + abs(minPos))))) + " \n")


def parse(path, fileName, outname):
    values = load_SK_bin_phase_confidence_vertices(path + "/" + fileName)
    save_image(path + "/" + outname + "_conf", values["conf"])
    save_image(path + "/" + outname + "_phase", values["phase"])
    #save_image(path + "/" + outname + "_flags_WHICH" + ".png", values["flags"]) # booleans TODO for keys in values["flags"] save image with right name
    save_PointCloud(path + "/" + outname + "_vertices", values["vertices"])


if __name__ == "__main__":
    parse(".", "DS326_000_1_89.bin", "DELETEMEPLEASE")