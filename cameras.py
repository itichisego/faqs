import os
import json
import datetime
import subprocess
import time

import cv2
import numpy as np

from pySimpleKinect import KinectCamera

#import pip
#pip.main(['install','rawpy'])
import rawpy

from utils import DEBUG

class CameraObject(object):
    type = "NONAME"
    saveFolder = "RGB"
    photoNumber = 1
    offsets = {"x": 0, "y": 0, "z": 0}
    currentPhotoNumber = 1
    cameraNumber = 0
    softwarePath = ""

    def getType(self):
        return self.type

    def setSaveFolder(self, folderName):
        self.saveFolder = folderName

    def setPhotoNumber(self, number):
        self.photoNumber = number

    def getPhotoNumber(self):
        return self.photoNumber

    def setOffsets(self, x, y, z):
        self.offsets["x"] = x
        self.offsets["y"] = y
        self.offsets["z"] = z

    def setSoftwarePath(self, path):
        self.softwarePath = path

    def makeRootFolder(self):
        if not os.path.exists(self.type):
            os.makedirs(self.type)

    def setCameraNumber(self, cameraNumber):
        self.cameraNumber = cameraNumber

    def makeFolder(self):
        self.saveFolder = self.type + "/" + str(self.cameraNumber).zfill(3)
        if not os.path.exists(self.saveFolder):
            os.makedirs(self.saveFolder)

    def saveJSON(self, photoName):
        jsonValue = {}
        jsonValue["Camera"] = self.saveFolder
        jsonValue["CameraNumber"] = self.cameraNumber
        jsonValue["fileName"] = photoName
        # TODO add others values
        fileName = "Metadata_" + str(self.currentPhotoNumber).zfill(5) + "_" + str(self.photoNumber)
        #print(fileName)
        #print(self.saveFolder + "/" + fileName + ".json", "w")
        with open(self.saveFolder + "/" + fileName + ".json", "w") as f:
            json.dump(jsonValue, f, indent=4)

    def takePhoto(self, zposition):
        raise NotImplementedError("Camera of type " + self.type + " has not implemented the takePhoto method.")

    def cleanUp(self):
        pass

class RGB(CameraObject):
    type = "RGB"
    def takePhoto(self, zposition):
        print("DigiCamControl running")
        timestamp = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
        fileName = self.type + "_" + str(self.cameraNumber).zfill(3) + \
                   "_" + zposition + "z_" + str(self.currentPhotoNumber).zfill(4) + \
                   "_" + str(self.photoNumber).zfill(4)# + \
        self.saveJSON(fileName)

        args1 = self.softwarePath + "CameraControlCmd.exe" \
               + " /filename " + self.saveFolder + "\\jpg\\" + fileName + ".jpg" \
               + " /compression JPEG" \
               + " /iso 100" \
               + " /shutter \"1s\"" \
               + " /aperture 36" \
               + " /capturenoaf"

        args2 = self.softwarePath + "CameraControlCmd.exe" \
                + " /filename " + self.saveFolder + "\\raw\\" + fileName + ".nef" \
                + " /compression RAW" \
                + " /iso 100" \
                + " /shutter \"2s\"" \
                + " /aperture 29" \
                + " /capturenoaf"

        args3 = self.softwarePath + "CameraControlCmd.exe" \
                + " /filename " + self.saveFolder + "\\raw\\" + fileName + ".nef" \
                + " /compression RAW" \
                + " /iso 160" \
                + " /shutter \"1/5s\"" \
                + " /aperture 16" \
                + " /capturenoaf" # zoom un rien après 18 et autofocus: 3

        #whitebalance

        #C:\\Program Files (x86)\\digiCamControl\\CameraControlCmd.exe /filename " + self.os_path_cpp + file_name + \
        #                       "_" + str(self.camera_number) + "_" + str(picture_per_position) + ".raw /capturenoaf /compression RAW_(FINE) /iso 100 /shutter \"1/1.3\" /aperture 22"

        #CameraControlCmd.exe /filename H:\test.jpg /capturenoaf /compression JPG /iso 100 /shutter "1/1.3" /aperture 22

        #CameraControlCmd.exe /filenameTemplate H:\testi_iii_lala /capturenoaf /compression RAW_+_JPEG /iso 100 /shutter "1/1.3" /aperture 22

        #subprocess.call(args1, stdout=None, stderr=None, shell=False)
        subprocess.call(args3, stdout=None, stderr=None, shell=False)

        #self._saveImageAsRaw(fileName)

        self.photoNumber += 1
        #time.sleep(2)
    def _saveImageAsRaw(self, fileName):
        imagePath = self.saveFolder + "\\raw\\" + fileName + ".nef"
        raw = rawpy.imread(imagePath)
        rgb = raw.postprocess() # TODO check parameters
        brg = cv2.cvtColor(rgb, cv2.COLOR_RGB2BGR)
        cv2.imwrite(self.saveFolder + "\\" + fileName + ".png",brg)

class DS326(CameraObject):
    type = "DS326"
    options = "-c -p -v"
    depthConfig = "ID_0146_0102_25FPS"
    numberFrames = 50

    def setOptions(self, options="-c -p -v"):
        self.options = options

    def setDepthConfig(self, config="ID_0146_0102_25FPS"):
        self.depthConfig = config

    def setNumberFrames(self, number=50):
        self.numberFrames = number

    def takePhoto(self, zposition):
        print("DS326 running")
        time.sleep(1) # stabilization time
        timestamp = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
        fileName = self.type + "_" + str(self.cameraNumber).zfill(3) + \
                   "_" + zposition + "z_" + str(self.currentPhotoNumber).zfill(4) + \
                   "_" + str(self.photoNumber).zfill(4) + \
                   ".bin"
        self.saveJSON(fileName)
        args = self.softwarePath + "DepthSenseBenchmark.exe" \
               " --depth --depth-record-raw " + self.saveFolder + "\\" + fileName + " -q " + self.options + \
               " --max-samples " + str(self.numberFrames) + \
               " --confidence-threshold 0 "
        if self.depthConfig != None:
            args += " --depth-config " + self.depthConfig
        print(args)
        subprocess.call(args, stdout=None, stderr=None, shell=False)
        self.photoNumber += 1


class Kinect(CameraObject):
    type = "Kinect"

    def __init__(self):
        self.kinect = KinectCamera()
        self.kinect.open()

    def __del__(self):
        #self.kinect.close()
        del self.kinect

    def takePhoto(self, zposition):
        print("Kinect running")

        time.sleep(1) #stabilisation time

        if self.z_tmp < 0:
            self.z_tmp = float(zposition)
            self.createFolder("texture\\" + str(zposition))
            self.createFolder("depth\\" + str(zposition))
            self.createFolder("depth_mapped\\" + str(zposition))
            self.createFolder("mask\\" + str(zposition))
        else:
            if self.z_tmp != zposition:
                self.z_tmp = float(zposition)
                self.createFolder("texture\\" + str(zposition))
                self.createFolder("depth\\" + str(zposition))
                self.createFolder("depth_mapped\\" + str(zposition))
                self.createFolder("mask\\" + str(zposition))

        folder = str(self.z_tmp)

        timestamp = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

        fileName = str(self.cameraNumber).zfill(3) + \
                "_" + zposition + "z_" + str(self.currentPhotoNumber).zfill(4) + \
                "_" + str(self.photoNumber).zfill(4) + \
                ".png"

        while not self.kinect.nextFrame():
            continue

        #self.saveJSON(fileName)
        self.kinect.save_texture(self.saveFolder + "\\texture\\" + folder + "\\" + self.type + "_out_texture_" + fileName)
        self.flipY(self.saveFolder + "\\texture\\" + folder + "\\"  + self.type + "_out_texture_" + fileName, cv2.IMREAD_ANYCOLOR)

        # self.kinect.save_infrared(self.saveFolder + "\\" + folder + "\\"  + self.type + "_out_IR_" + fileName)
        # self.kinect.save_infrared_mapped(self.saveFolder + "\\" + folder + "\\"  + self.type + "_out_IR_mapped_" + fileName)

        self.kinect.save_depth_mapped(self.saveFolder + "\\depth_mapped\\" + folder + "\\"  + self.type + "_out_depth_mapped_" + fileName)
        self.flipY(self.saveFolder + "\\depth_mapped\\" + folder + "\\"  + self.type + "_out_depth_mapped_" + fileName,cv2.IMREAD_ANYDEPTH)

        self.kinect.save_depth(self.saveFolder + "\\depth\\" + folder + "\\"  + self.type + "_out_depth_" + fileName)
        self.flipY(self.saveFolder + "\\depth\\" + folder + "\\"  + self.type + "_out_depth_" + fileName, cv2.IMREAD_ANYDEPTH)

        self.kinect.save_mask(self.saveFolder + "\\mask\\" + folder + "\\"  + self.type + "_out_mask_" + fileName)
        self.flipY(self.saveFolder + "\\mask\\" + folder + "\\"  + self.type + "_out_mask_" + fileName, cv2.IMREAD_ANYCOLOR)


        ## AVERAGE DEPTH
        #self.averageDepth(50, fileName)

        #self.saveUint8Image("_out_depth_", "depth",  fileName)
        #self.saveUint8Image("_out_depth_mapped_", "depth_mapped", fileName)

        self.photoNumber += 1


    def makeFolder(self):
        super(Kinect, self).makeFolder()

        self.z_tmp = -10.0

        self.createFolder("texture")
        self.createFolder("depth")
        self.createFolder("depth_mapped")
        self.createFolder("mask")

    def createFolder(self, folderName):
        if not os.path.exists(self.saveFolder + "/" + folderName):
            os.makedirs(self.saveFolder + "/" + folderName)



    def saveUint8Image(self, pre, subfolder, fileName):
        image = cv2.imread(self.saveFolder + "\\" + subfolder + "\\" + self.type + pre + fileName, flags=cv2.IMREAD_ANYDEPTH)
        minmax = cv2.minMaxLoc(image)

        min_depth = minmax[1] / 8.0
        max_depth = minmax[1] / 6.0

        img = np.empty(image.shape, 'uint8')
        alpha = 255.0 / (max_depth - min_depth)
        beta = -alpha * min_depth
        cv2.convertScaleAbs(image, img, alpha, beta)
        cv2.imwrite(self.saveFolder + "\\" + self.type + pre + "_converted_uint8_" + fileName, img)

    def flipY(self, fileName, flag):
        image = cv2.imread(fileName, flags=flag)
        img = cv2.flip(image,1)
        cv2.imwrite(fileName, img)

    def averageDepth(self, number, fileName):
        average_image = np.zeros((424, 512), np.uint64)
        for photo in range(number) :
            while not self.kinect.nextFrame():
                continue
            self.kinect.save_depth(self.saveFolder + "\\" + self.type + "_out_depth_" + fileName + "_" + str(photo).zfill(2))

            source = cv2.imread(self.saveFolder + "\\" + self.type + "_out_depth_" + fileName + "_" + str(photo).zfill(2), flags=cv2.IMREAD_ANYDEPTH)
            average_image = average_image + source
            #TODO delete temporary images

            average_image = average_image/50
            minmax = cv2.minMaxLoc(average_image)
            average_image = average_image/minmax[1]
            average_image = average_image*255

            cv2.imwrite(self.saveFolder + "\\" + self.type + "_average_out_depth_" + fileName, average_image)
            self.saveUint8Image("_average_out_depth_", fileName)


class CamerasHolder:
    cameras = {}

    def __init__(self):
        pass

    def keys(self):
        return self.cameras.keys()

    def add(self, cameraObj, photoNumber, softwarePath):
        camera = cameraObj()
        type = camera.getType()
        camera.setSoftwarePath(softwarePath)

        if type not in self.cameras:
            self.cameras[type] = []
            # root folder for this type of camera
            camera.makeRootFolder()
        # sub folder for this type of camera RGB/001
        size = len(self.cameras[type])
        camera.makeFolder()
        camera.setPhotoNumber(photoNumber) # how many photos to take
        self.cameras[type].append(camera)
        self.cameras[type][size-1].setCameraNumber(size)

    def takePhoto(self, type, photoNumber):
        #for camera in self.cameras[type]:
        self.camera.takePhoto()

    def getCameras(self, type):
        return self.cameras[type]

    def showCameras(self):
        if True: #TODO DEBUG
            print("Cameras registred")
            for cameraType in self.cameras:
                print("Camera of type " + cameraType + "(" \
                      + str(len(self.cameras[cameraType])) + "): [")
                for cam in self.cameras[cameraType]:
                    print(str(cam.getPhotoNumber()) + " ")
                print("]")
